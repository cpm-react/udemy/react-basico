import Comentario from './components/Comentario/Comentario'
import Formulario from './components/Formulario/Formulario'
import MeuComponente from './components/MeuComponente/MeuComonente'
import './App.css';
import { Component } from 'react';

class App extends Component {

  state = {
    comentarios: [
      {
        nome: "Cassiano",
        email: "cassiano@gmail.com",
        data: new Date(2001, 0, 1, 1, 1, 1),
        mensagem: "Minha primeira Mensagem"
      }, 
      {
        nome: "Nome 2",
        email: "dois@gmail.com",
        data: new Date(2002, 1, 2, 2, 2, 2),
        mensagem: "Minha segunda Mensagem"
      },
      {
        nome: "Três",
        email: "3@gmail.com",
        data: new Date(2021, 5, 21, 23, 32, 23),
        mensagem: "Minha terceira Mensagem"
      }
    ],
    novoComentario: {
      nome: "",
      email: "",
      mensagem: ""
    }
  }

  adicionarComentario = (event) => {
    event.preventDefault()
    const novoComentario = { ...this.state.novoComentario, data: new Date() }

    this.setState({
      comentarios: [...this.state.comentarios, novoComentario],
      novoComentario: {
        nome: "",
        email: "",
        mensagem: ""
      }});
  }

  digitacaoRemota = event => {
    let {value, name} = event.target
    this.setState({novoComentario: {...this.state.novoComentario, [name]: value}});
  }

  apagarComentario = comentario => {
    let lista = this.state.comentarios.filter(c => c !== comentario)
    
    this.setState({
      comentarios: lista
    })
  }

  render() {
    return (
      <div className="App">
        <h1>Meu Projeto</h1>
        <MeuComponente
          titulo="Olá"
        ></MeuComponente>
        {this.state.comentarios.map((comentario, i) => (
          <Comentario 
            key={i}
            nome={comentario.nome}
            email={comentario.email}
            data={comentario.data}
            onRemove={this.apagarComentario.bind(this, comentario)}>
            {comentario.mensagem}
          </Comentario>
        ))}

        <Formulario
          onSubmit={this.adicionarComentario}
          onChange={this.digitacaoRemota}
          novoComentario={this.state.novoComentario}
        ></Formulario>
      </div>
    );
  }
}

export default App;
