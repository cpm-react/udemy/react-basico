import React from 'react'
import { formatRelative } from 'date-fns'
import { ptBR } from 'date-fns/locale'

import './Comentario.css'
import imagemUsuario from './0.png'

const Comentario = (props) => {

    return (
        <div className='Comentario'>
            <img className="imagem" src={imagemUsuario} alt={props.nome}/>

            <div className="conteudo">
                <h3 className="nome">{props.nome}</h3>
                <p className="email">{props.email}</p>
                <h4 className="mensagem">{props.children}</h4>
                <p className="data">{formatRelative(props.data, new Date(), {locale: ptBR})}</p>
                <button className="botao" onClick={props.onRemove}>x</button>
            </div>

        </div>
    )
};

export default Comentario;