import React, { Component } from 'react'

import './Formulario.css'

class Formulario extends Component {

    render() {
        return (
            <form method="post" className="formulario" onSubmit={this.props.onSubmit}>
                <h4>Novo comentário</h4>
                <div>
                    <input
                    type="text"
                    placeholder="Nome"
                    name="nome"
                    id="nome"
                    value={this.props.novoComentario.nome}
                    onChange={this.props.onChange}
                    required/>
                </div>
                <div>
                    <input
                    type="email"
                    placeholder="email"
                    name="email"
                    id="email"
                    value={this.props.novoComentario.email}
                    onChange={this.props.onChange}
                    required/>
                </div>
                <div>
                    <textarea
                    placeholder="Mensagem"
                    name="mensagem"
                    id="mensagem"
                    value={this.props.novoComentario.mensagem}
                    onChange={this.props.onChange}
                    required></textarea>
                </div>
                <button type="submit">Add Comentário</button>
            </form>
        )
    }
}

export default Formulario