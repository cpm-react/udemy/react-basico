import { addMinutes } from 'date-fns'
import React, { Component } from 'react'

class MeuComponente extends Component {
    constructor(props) {
        console.log("construtor")
        super(props)

        this.state = { numero: 0}
    }

    static getDerivedStateFromProps(props, state) {
        console.log("getDerivedStateFromProps")

        return null
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log("shouldComponentUpdate")

        console.log("Estado atual...: ", this.state)
        console.log("Próximo estado.: ", nextState)

        return nextState.numero % 2 === 0
    }

    componentDidUpdate() {
        console.log("shouldComponentUpdate")
    }

    componentDidMount() {
        console.log("componentDidMount")
    }

    addNum() {
        console.log("addNum -> setState")
        let numAtual = this.state.numero
        numAtual+=1
        this.setState({ numero: numAtual})
    }

    render() {
        console.log("render")
        return (
            <div>
                <p>{this.props.titulo}</p>
                <h3>{this.state.numero}</h3>
                <button onClick={this.addNum.bind(this)}>Add Num</button>
                <br/><br/><br/><br/>
            </div>
        )
    }
}


export default MeuComponente